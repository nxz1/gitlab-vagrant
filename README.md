# Vagrant to quickly spin up a gitlab instance

### 1. Clone the repository
    $ git clone https://gitlab.com/nxz1/gitlab-vagrant
    $ cd gitlab-vagrant

### 2. Bring up the VM (default provider: virtualbox)
    $ vagrant status - check the name of the machine
    $ vagrant up - bring up the machine

### 3. Vagrant SSH 
    $ vagrant ssh gitlab-instance-01

*If working fine
    $ exit

### 4. Check if our admin user is created
    $ vagrant ssh gitlab-instance-01
    $ cat /etc/passwd
    $ exit
    $ ssh admin@172.16.16.101

*If working fine
    $ exit

### 5. Check sudo privileges for our admin user
    $ ssh admin@172.16.16.101
    $ sudo apt update

### 6. Start Gitlab Installation
